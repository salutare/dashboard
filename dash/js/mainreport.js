FusionCharts.ready(function () {
    var analysisChart = new FusionCharts({
        type: 'stackedColumn3DLine',
        renderAt: 'chart-container',
        width: '500',
        height: '350',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "showvalues": "0",
                "caption": "Cost Analysis",
                "numberprefix": "$",
                "xaxisname": "Quarters",
                "yaxisname": "Cost",
                "showBorder": "0",
                "paletteColors": "#0075c2,#1aaf5d,#f2c500",
                "bgColor": "#ffffff",
                "canvasBgColor": "#ffffff",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "divlineColor": "#999999",
                "divLineIsDashed": "1",
                "divLineDashLen": "1",
                "divLineGapLen": "1",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                "legendBgColor": "#ffffff",
                "legendBorderAlpha": '0',
                "legendShadow": '0',
                "legendItemFontSize": '10',
                "legendItemFontColor": '#666666'
            },
            "categories": [
                {
                    "category": [
                        {
                            "label": "Quarter 1"
                        },
                        {
                            "label": "Quarter 2"
                        },
                        {
                            "label": "Quarter 3"
                        },
                        {
                            "label": "Quarter 4"
                        }
                    ]
                }
            ],
            "dataset": [
                {
                    "seriesname": "Fixed Cost",
                    "data": [
                        {
                            "value": "235000"
                        },
                        {
                            "value": "225100"
                        },
                        {
                            "value": "222000"
                        },
                        {
                            "value": "230500"
                        }
                    ]
                },
                {
                    "seriesname": "Variable Cost",
                    "data": [
                        {
                            "value": "230000"
                        },
                        {
                            "value": "143000"
                        },
                        {
                            "value": "198000"
                        },
                        {
                            "value": "327600"
                        }
                    ]
                },
                {
                    "seriesname": "Budgeted cost",
                    "renderas": "Line",
                    "data": [
                        {
                            "value": "455000"
                        },
                        {
                            "value": "334000"
                        },
                        {
                            "value": "426000"
                        },
                        {
                            "value": "403000"
                        }
                    ]
                }
            ]
        }
    }).render();
});

FusionCharts.ready(function(){
    var revenueChart = new FusionCharts({
        "type": "column2d",
        "renderAt": "chartContainer",
        "width": "500",
        "height": "300",
        "dataFormat": "json",
        "dataSource":  {
          "chart": {
            "caption": "Recetas por Mes",
            "subCaption": "Recetas Dispensadas por Mes",
            "xAxisName": "Month",
            "yAxisName": "Revenues (In USD)",
            "theme": "fint"
         },
         "data": [
            {
               "label": "Jan",
               "value": "420000"
            },
            {
               "label": "Feb",
               "value": "810000"
            },
            {
               "label": "Mar",
               "value": "720000"
            },
            {
               "label": "Apr",
               "value": "550000"
            },
            {
               "label": "May",
               "value": "910000"
            },
            {
               "label": "Jun",
               "value": "510000"
            },
            {
               "label": "Jul",
               "value": "680000"
            },
            {
               "label": "Aug",
               "value": "620000"
            },
            {
               "label": "Sep",
               "value": "610000"
            },
            {
               "label": "Oct",
               "value": "490000"
            },
            {
               "label": "Nov",
               "value": "900000"
            },
            {
               "label": "Dec",
               "value": "730000"
            }
          ]
      }

  });
revenueChart.render();
})

FusionCharts.ready(function () {
    var conversionChart = new FusionCharts({
        type: 'bubble',
        renderAt: 'chart-container1',
        width: '600',
        height: '300',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Sales Analysis of Shoe Brands",
                "subcaption": "Last Quarter",
                "xAxisMinValue": "0",
                "xAxisMaxValue": "100",
                "yAxisMinValue": "0",
                "yAxisMaxValue": "30000",
                "plotFillAlpha": "70",
                "plotFillHoverColor": "#6baa01",
                "showPlotBorder": "0",
                "xAxisName": "Average Price",
                "yAxisName": "Units Sold",
                "numDivlines": "2",
                "showValues":"1",
                "showTrendlineLabels": "0",
                "plotTooltext": "$name : Profit Contribution - $zvalue%",
                "drawQuadrant" : "1",
                "quadrantLineAlpha" : "80",
                "quadrantLineThickness" : "3",
                "quadrantXVal" : "50",
                "quadrantYVal": "15000",
                //Quadrant Labels
                "quadrantLabelTL": "Low Price / High Sale",
                "quadrantLabelTR": "High Price / High Sale",
                "quadrantLabelBL": "Low Price / Low Sale",
                "quadrantLabelBR": "High Price / Low Sale",
                  
                //Cosmetics
                "baseFontColor" : "#333333",
                "baseFont" : "Helvetica Neue,Arial",
                "captionFontSize" : "14",
                "subcaptionFontSize" : "14",
                "subcaptionFontBold" : "0",
                "showBorder" : "0",
                "bgColor" : "#ffffff",
                "showShadow" : "0",
                "canvasBgColor" : "#ffffff",
                "canvasBorderAlpha" : "0",
                "divlineAlpha" : "100",
                "divlineColor" : "#999999",
                "divlineThickness" : "1",
                "divLineIsDashed" : "1",
                "divLineDashLen" : "1",
                "divLineGapLen" : "1",
                "use3dlighting" : "0",
                "showplotborder" : "0",
                "showYAxisLine" : "1",
                "yAxisLineThickness" : "1",
                "yAxisLineColor" : "#999999",
                "showXAxisLine" : "1",
                "xAxisLineThickness" : "1",
                "xAxisLineColor" : "#999999",
                "showAlternateHGridColor" : "0",
                "showAlternateVGridColor" : "0"
                
            },
            "categories": [
                {
                    "category": [
                        {
                            "label": "$0",
                            "x": "0"
                        }, 
                        {
                            "label": "$20",
                            "x": "20",
                            "showverticalline": "1"
                        }, 
                        {
                            "label": "$40",
                            "x": "40",
                            "showverticalline": "1"
                        }, 
                        {
                            "label": "$60",
                            "x": "60",
                            "showverticalline": "1"
                        }, 
                        {
                            "label": "$80",
                            "x": "80",
                            "showverticalline": "1"
                        }, {
                            "label": "$100",
                            "x": "100",
                            "showverticalline": "1"
                        }
                    ]
                }
            ],
            "dataset": [
                {
                    "color":"#00aee4",
                    "data": [
                        {
                            "x": "80",
                            "y": "15000",
                            "z": "24",
                            "name": "Nike"
                        }, 
                        {
                            "x": "60",
                            "y": "18500",
                            "z": "26",
                            "name": "Adidas"
                        }, 
                        {
                            "x": "50",
                            "y": "19450",
                            "z": "19",
                            "name": "Puma"
                        }, 
                        {
                            "x": "65",
                            "y": "10500",
                            "z": "8",
                            "name": "Fila"
                        }, 
                        {
                            "x": "43",
                            "y": "8750",
                            "z": "5",
                            "name": "Lotto"
                        }, 
                        {
                            "x": "32",
                            "y": "22000",
                            "z": "10",
                            "name": "Reebok"
                        }, 
                        {
                            "x": "44",
                            "y": "13000",
                            "z": "9",
                            "name": "Woodland"
                        }
                    ]
                }
            ],
            "trendlines": [
                {
                    "line": [
                        {
                            "startValue": "20000",
                            "endValue": "30000",
                            "isTrendZone": "1",
                            "color": "#aaaaaa",
                            "alpha": "14"
                        }, 
                        {
                            "startValue": "10000",
                            "endValue": "20000",
                            "isTrendZone": "1",
                            "color": "#aaaaaa",
                            "alpha": "7"
                        }
                    ]
                }
            ],
            "vTrendlines": [
                {
                    "line": [
                        {
                            "startValue": "44",
                            "isTrendZone": "0",
                            "color": "#0066cc",
                            "thickness" : "1",
                            "dashed" : "1",
                            "displayValue": "Gross Avg."
                        }
                    ]
                }
            ]
        }
    });
    conversionChart.render();
});


FusionCharts.ready(function () {
    var salesAnlysisChart = new FusionCharts({
        type: 'mscombi2d',
        renderAt: 'chart-container2',
        width: '600',
        height: '300',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Dispensación de Recetas",
                "subCaption": "Evolución de dispensación en las farmacias",
                "xAxisname": "Mes",
                "yAxisName": "Monto (En S/.)",
                "numberPrefix": "$",
                "showBorder": "0",
                "showValues": "0",
                "paletteColors": "#0075c2,#1aaf5d,#f2c500",
                "bgColor": "#ffffff",
                "showCanvasBorder": "0",
                "canvasBgColor": "#ffffff",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "divlineColor": "#999999",
                "divLineIsDashed": "1",
                "divLineDashLen": "1",
                "divLineGapLen": "1",
                "showAlternateHGridColor": "0",
                "usePlotGradientColor": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                "legendBgColor": "#ffffff",
                "legendBorderAlpha": '0',
                "legendShadow": '0',
                "legendItemFontSize": '10',
                "legendItemFontColor": '#666666'
            },
            "categories": [
                {
                    "category": [
                        {
                            "label": "Jan"
                        },
                        {
                            "label": "Feb"
                        },
                        {
                            "label": "Mar"
                        },
                        {
                            "label": "Apr"
                        },
                        {
                            "label": "Mai"
                        },
                        {
                            "label": "Jun"
                        },
                        {
                            "label": "Jul"
                        },
                        {
                            "label": "Aug"
                        },
                        {
                            "label": "Sep"
                        },
                        {
                            "label": "Oct"
                        },
                        {
                            "label": "Nov"
                        },
                        {
                            "label": "Dec"
                        }
                    ]
                }
            ],
            "dataset": [
                {
                    "seriesName": "Medicina stock total",
                    "showValues": "1",
                    "data": [
                        {
                            "value": "16000"
                        },
                        {
                            "value": "20000"
                        },
                        {
                            "value": "18000"
                        },
                        {
                            "value": "19000"
                        },
                        {
                            "value": "15000"
                        },
                        {
                            "value": "21000"
                        },
                        {
                            "value": "16000"
                        },
                        {
                            "value": "20000"
                        },
                        {
                            "value": "17000"
                        },
                        {
                            "value": "25000"
                        },
                        {
                            "value": "19000"
                        },
                        {
                            "value": "23000"
                        }
                    ]
                },
                {
                    "seriesName": "Projeción de Consumo",
                    "renderAs": "line",
                    "data": [
                        {
                            "value": "15000"
                        },
                        {
                            "value": "16000"
                        },
                        {
                            "value": "17000"
                        },
                        {
                            "value": "18000"
                        },
                        {
                            "value": "19000"
                        },
                        {
                            "value": "19000"
                        },
                        {
                            "value": "19000"
                        },
                        {
                            "value": "19000"
                        },
                        {
                            "value": "20000"
                        },
                        {
                            "value": "21000"
                        },
                        {
                            "value": "22000"
                        },
                        {
                            "value": "23000"
                        }
                    ]
                },
                {
                    "seriesName": "Qantidad de Dispensación",
                    "renderAs": "area",
                    "data": [
                        {
                            "value": "4000"
                        },
                        {
                            "value": "5000"
                        },
                        {
                            "value": "3000"
                        },
                        {
                            "value": "4000"
                        },
                        {
                            "value": "1000"
                        },
                        {
                            "value": "7000"
                        },
                        {
                            "value": "1000"
                        },
                        {
                            "value": "4000"
                        },
                        {
                            "value": "1000"
                        },
                        {
                            "value": "8000"
                        },
                        {
                            "value": "2000"
                        },
                        {
                            "value": "7000"
                        }
                    ]
                }
            ]
        }
    }).render();
});

FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'doughnut3d',
        renderAt: 'chart-container3',
        width: '450',
        height: '300',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Dispensación de Medicinas por Categoria",
                "subCaption": "Ano 2016",
                "numberPrefix": "$",
                "paletteColors": "#0075c2,#1aaf5d,#f2c500,#f45b00,#8e0000",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "use3DLighting": "0",
                "showShadow": "0",
                "enableSmartLabels": "0",
                "startingAngle": "310",
                "showLabels": "0",
                "showPercentValues": "1",
                "showLegend": "1",
                "legendShadow": "0",
                "legendBorderAlpha": "0",                                
                "decimals": "0",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
            },
            "data": [
                {
                    "label": "Controlados",
                    "value": "28504"
                }, 
                {
                    "label": "Termolabeis",
                    "value": "14633"
                }, 
                {
                    "label": "Cosmeticos",
                    "value": "10507"
                }, 
                {
                    "label": "Saneantes",
                    "value": "4910"
                }
            ]
        }
    }).render();
});