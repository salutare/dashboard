<?php

/*Include the `fusioncharts.php` file that contains functions
	to embed the charts.
*/
 

  /* The following 4 code lines contain the database connection information. Alternatively, you can move these code lines to a separate file and include the file here. You can also modify this code based on your database connection.   */

   $hostdb = "localhost";  // MySQl host
   $userdb = "root";  // MySQL username
   $passdb = "";  // MySQL password
   $namedb = "ihs";  // MySQL database name

   // Establish a connection to the database
   $dbhandle = new mysqli($hostdb, $userdb, $passdb, $namedb);

   // Render an error message, to avoid abrupt failure, if the database connection parameters are incorrect
   if ($dbhandle->connect_error) {
  	exit("There was an error with your connection: ".$dbhandle->connect_error);
   }

?>

<html>
   <head>
  	<title>FusionCharts XT - Column 2D Chart - Data from a database</title>
	  <link  rel="stylesheet" type="text/css" href="css/style.css" />

	<!--  Include the `fusioncharts.js` file. This file is needed to render the chart. Ensure that the path to this JS file is correct. Otherwise, it may lead to JavaScript errors. -->

      <script type="text/javascript" src="fusioncharts/js/fusioncharts.js"></script>
	  <script type="text/javascript" src="fusioncharts/js/themes/fusioncharts.theme.fint.js"></script>
      
      
   </head>
   <body>
  	<?php

     	// Form the SQL query that returns the top 10 most populous countries
     	//$strQuery = "SELECT Name, Population, Code FROM Country ORDER BY Population DESC LIMIT 10";

        $strQuery = "select fecha  from dsh_exceso group by fecha Limit 10";
	    	 	// Execute the query, or else return the error message.
    	 $result = $dbhandle->query($strQuery) or exit("Error code ({$dbhandle->errno}): {$dbhandle->error}");


     	// If the query returns a valid response, prepare the JSON string
     	if ($result) {
        	// The `$arrData` array holds the chart attributes and data
        	$arrData = array(
                "chart" => array(
                    "caption" =>  "INDICADOR DE ABASTECIMIENTO EXCESO DE MEDICAMENTOS",
			        "subCaption" => "Asesor: Elber Mestanza",
			        "xAxisname" => "Fechas",
			        "yAxisName"=> "% Medicamentos",
			        "numberPrefix"=> "",
			        "stack100Percent"=> "1",
			        "decimals"=> "1",
			        "showPercentInTooltip"=> "1",
			        "showValues"=> "1",
			        "showPercentValues"=> "0",
			        "valueFontColor"=> "#ffffff",
			        "theme"=> "fint"
              	)
           	);

        	
        	//$arrData["categories"] = array("category" => array(array("label" => "Q1"),array("label" => "Q2"),array("label" => "Q3"),array("label" => "Q4")));
        	
        	
        	$arrData["categories"] = array("category" => array());
        	
        	
        	while($row = mysqli_fetch_array($result)) 
        	{
           	array_push($arrData["categories"]["category"], array(
                "label" => $row["fecha"]
              	)
           	);
        	}
        	
        	
        	$strQuery = "select sum(valor) valor, fecha,id_asesor ,id_bandeira from dsh_exceso where id_asesor = 1 and id_bandeira = 1  group by fecha order by fecha Limit 10";
	    	// Execute the query, or else return the error message.
    	 	$result = $dbhandle->query($strQuery) or exit("Error code ({$dbhandle->errno}): {$dbhandle->error}");
        	$arrData["dataset"] = array();
        	$arrData["dataset"][0] = array("seriesname" => "Cero", "data" => array());
        	while($row = mysqli_fetch_array($result)) 
        	{
           		array_push($arrData["dataset"][0]["data"], array(
                "value" => $row["valor"]
              	)
           	);
        	}
        	
        	$arrData["dataset"][1] = array("seriesname" => "Cero", "data" => array());
        	$strQuery = "select sum(valor) valor, fecha,id_asesor ,id_bandeira from dsh_exceso where id_asesor = 1 and id_bandeira = 2  group by fecha order by fecha Limit 10";
	    	// Execute the query, or else return the error message.
    	 	$result = $dbhandle->query($strQuery) or exit("Error code ({$dbhandle->errno}): {$dbhandle->error}");
        	$arrData["dataset"][1] = array("seriesname" => "Muy Bajo", "data" => array());
        	while($row = mysqli_fetch_array($result)) 
        	{
           		array_push($arrData["dataset"][1]["data"], array(
                "value" => $row["valor"]
              	)
           	);
        	}
        	
        	
        	$arrData["dataset"][2] = array("seriesname" => "Cero", "data" => array());
        	$strQuery = "select sum(valor) valor, fecha,id_asesor ,id_bandeira from dsh_exceso where id_asesor = 1 and id_bandeira = 3  group by fecha order by fecha Limit 10";
	    	// Execute the query, or else return the error message.
    	 	$result = $dbhandle->query($strQuery) or exit("Error code ({$dbhandle->errno}): {$dbhandle->error}");
        	$arrData["dataset"][1] = array("seriesname" => "Minimo", "data" => array());
        	while($row = mysqli_fetch_array($result)) 
        	{
           		array_push($arrData["dataset"][2]["data"], array(
                "value" => $row["valor"]
              	)
           	);
        	}
        	
        	$arrData["dataset"][3] = array("seriesname" => "Cero", "data" => array());
        	$strQuery = "select sum(valor) valor, fecha,id_asesor ,id_bandeira from dsh_exceso where id_asesor = 1 and id_bandeira = 4  group by fecha order by fecha Limit 10";
	    	// Execute the query, or else return the error message.
    	 	$result = $dbhandle->query($strQuery) or exit("Error code ({$dbhandle->errno}): {$dbhandle->error}");
        	$arrData["dataset"][1] = array("seriesname" => "Ideal", "data" => array());
        	while($row = mysqli_fetch_array($result)) 
        	{
           		array_push($arrData["dataset"][3]["data"], array(
                "value" => $row["valor"]
              	)
           	);
        	}
        	
        	
        	$arrData["dataset"][4] = array("seriesname" => "Cero", "data" => array());
        	$strQuery = "select sum(valor) valor, fecha,id_asesor ,id_bandeira from dsh_exceso where id_asesor = 1 and id_bandeira = 5  group by fecha order by fecha Limit 10";
	    	// Execute the query, or else return the error message.
    	 	$result = $dbhandle->query($strQuery) or exit("Error code ({$dbhandle->errno}): {$dbhandle->error}");
        	$arrData["dataset"][1] = array("seriesname" => "Limite", "data" => array());
        	while($row = mysqli_fetch_array($result)) 
        	{
           		array_push($arrData["dataset"][4]["data"], array(
                "value" => $row["valor"]
              	)
           	);
        	}


			$arrData["dataset"][5] = array("seriesname" => "Cero", "data" => array());
			$strQuery = "select sum(valor) valor, fecha,id_asesor ,id_bandeira from dsh_exceso where id_asesor = 1 and id_bandeira = 6  group by fecha order by fecha Limit 10";
	    	// Execute the query, or else return the error message.
    	 	$result = $dbhandle->query($strQuery) or exit("Error code ({$dbhandle->errno}): {$dbhandle->error}");
        	$arrData["dataset"][1] = array("seriesname" => "Maximo 2", "data" => array());
        	while($row = mysqli_fetch_array($result)) 
        	{
           		array_push($arrData["dataset"][5]["data"], array(
                "value" => $row["valor"]
              	)
           	);
        	}
			


        	

	// Push the data into the array

//        	while($row = mysqli_fetch_array($result)) {
//           	array_push($arrData["data"], array(
//                "label" => $row["Name"],
//                "value" => $row["Population"],
//                "link" => "countryDrillDown.php?Country=".$row["Code"]
//              	)
//           	);
//        	}

        	/*JSON Encode the data to retrieve the string containing the JSON representation of the data in the array. */

        	$jsonEncodedData = json_encode($arrData);

			echo $jsonEncodedData;


        	/*Create an object for the column chart. Initialize this object using the FusionCharts PHP class constructor. The constructor is used to initialize the chart type, chart id, width, height, the div id of the chart container, the data format, and the data source. */

        	$columnChart = new FusionCharts("stackedcolumn2d", "myFirstChart" , 1200, 600, "chart-1", "json", $jsonEncodedData);

        	// Render the chart
        	$columnChart->render();

        	// Close the database connection
        	$dbhandle->close();

     	}

  	?>
  	<div id="chart-1"><!-- Fusion Charts will render here--></div>
   </body>
</html>