<!DOCTYPE html>
<html>


<?php
 include("fusioncharts.php");
 
 ?>

<head>
	<meta charset="utf-8">

	<title>AnythingSlider Simple Demo</title>
	<link rel="shortcut icon" href="demos/images/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" href="demos/images/apple-touch-icon.png">

	<!-- jQuery (required) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script>


	
	<link rel="stylesheet" href="css/theme-metallic.css">

	<!-- Anything Slider -->
	<link rel="stylesheet" href="css/anythingslider.css">
	<script src="js/jquery.anythingslider.js"></script>

	<!-- AnythingSlider optional extensions -->
	<!-- <script src="js/jquery.anythingslider.fx.js"></script> -->
	<!-- <script src="js/jquery.anythingslider.video.js"></script> -->

	<!-- Define slider dimensions here -->
	<style>
	#slider { width: 100%; height: 690px; }
	</style>

	<!-- AnythingSlider initialization -->
	<script>
		// DOM Ready
		$(function(){
			$('#slider').anythingSlider(
			{
				theme           : 'metallic',
				autoPlay     : true
			});
		});
	</script>

</head>

<body id="simple">


	<!-- Simple AnythingSlider -->

	<ul id="slider">

			<li class="panel1">
					<div>
						
						<?php
						 	include("dashtest2.php");
						?>
						
					</div>
				</li>
			 <li class="panel2">
					<div>
						<?php
							include("dshexceso.php");
						 	
						?>
					</div>
			</li>
				
				
	</ul>

	<!-- END AnythingSlider -->

</body>

</html>